# coding: utf-8

"""
    решение тестового задания http://yadi.sk/d/gPd_7q5sGutPx
    парсер событийного лога

    $ python parse.py <input_file> [<output_file>]

    если output_file не задан - вывод осуществляется в stdout
"""

import sys
import math
import heapq
from array import array
from itertools import groupby


START_REQUEST = 'StartRequest'
BACKEND_CONNECT = 'BackendConnect'
BACKEND_REQUEST = 'BackendRequest'
BACKEND_OK = 'BackendOk'
BACKEND_ERROR = 'BackendError'
START_MERGE = 'StartMerge'
START_SEND_RESULT = 'StartSendResult'
FINISH_REQUEST = 'FinishRequest'

SLOWLEST_SEND_HEAP_SIZE = 10 + 1


class Request(object):
    """
    Запрос
    """

    def __init__(self, rid, start_time):
        self.id = rid
        self.start_request_time = start_time
        self.backends = {}
        self.backend_requests = {}


class Backend(object):
    """
    Бекенд
    """

    __slots__ = ("group", "id", "errors", "requests")

    def __init__(self, group, url):
        self.group = group
        self.id = url
        self.errors = {}
        self.requests = 0


request_map = {}
backends_map = {}
request_times = array('I')
partial_merge_count = 0
slowlest_send_heap = []


def handle_start_request(timestamp, request_id, e):
    """
    Обработчик записи о событии START_REQUEST.
    """
    req = Request(request_id, int(timestamp))
    request_map[request_id] = req


def handle_be_connect(timestamp, request_id, e, group, url):
    """
    Обработчик записи о событии BACKEND_CONNECT.
    """
    req = request_map[request_id]
    backend = backends_map.get(url)
    if not backend:
        backend = Backend(group, url)
        backends_map[url] = backend
    # Ставим флаг, означающий что мы начали работу с бекендом из ГР "group"
    # в контексте запроса "request_id". Мы снимем флаг, когда получим ответ от этой ГР.
    # Если флаг не будет снят - это будет означать, что в данном запросе получены данные не от всех ГР. 
    req.backend_requests[group]  = 1
    req.backends[group] = backend


def handle_be_request(timestamp, request_id, e, group):
    """
    Обработчик записи о событии BACKEND_REQUEST.
    """
    req = request_map[request_id]
    # регистрируем обращение к бекенду
    req.backends[group].requests += 1


def handle_be_ok(timestamp, request_id, e, group):
    """
    Обработчик записи о событии BACKEND_OK.
    """
    req = request_map[request_id]
    # мы получили данные от бекенда ГР "group", снимаем флаг
    req.backend_requests[group] = 0


def handle_be_error(timestamp, request_id, e, group, error_msg):
    """
    Обработчик записи о событии BACKEND_ERROR.
    """
    req = request_map[request_id]
    backend = req.backends[group]
    # регистрируем ошибку для бекенда
    backend.errors[error_msg] = backend.errors.get(error_msg, 0) + 1


def handle_merge(timestamp, request_id, e):
    """
    Обработчик записи о событии START_MERGE.
    """
    pass


def handle_send_result(timestamp, request_id, e):
    """
    Обработчик записи о событии START_SEND_RESULT.
    """
    req = request_map[request_id]
    # регистрируем время события
    req.start_send_time = int(timestamp)


def handle_finish(timestamp, request_id, e):
    """
    Обработчик записи о событии FINISH_REQUEST.
    """
    req = request_map[request_id]
    # замеряем полное время обработки запроса
    # и время, которое заняла фаза отправки данных 
    total_time = int(timestamp) - req.start_request_time
    send_time = int(timestamp) - req.start_send_time
    # отмечаем неполный мердж
    if any(req.backend_requests.values()):
        global partial_merge_count
        partial_merge_count += 1
    request_times.append(total_time)

    if len(slowlest_send_heap) < SLOWLEST_SEND_HEAP_SIZE:
        heapq.heappush(slowlest_send_heap, (send_time, req.id))
    else:
        heapq.heapreplace(slowlest_send_heap, (send_time, req.id))
    del request_map[request_id]


event_handlers = {
    START_REQUEST: handle_start_request,
    BACKEND_CONNECT: handle_be_connect,
    BACKEND_REQUEST: handle_be_request,
    BACKEND_OK: handle_be_ok,
    BACKEND_ERROR: handle_be_error,
    START_MERGE: handle_merge,
    START_SEND_RESULT: handle_send_result,
    FINISH_REQUEST: handle_finish
}


def percentile(values, percent):
    """
    Вычисление перцентиля.
    """
    if not values:
        return None
    k = (len(values)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return values[int(k)]
    d0 = values[int(f)] * (c - k)
    d1 = values[int(c)] * (k - f)
    return d0 + d1


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "\tevent log parser"
        print "\tusage: python parse.py <input_file> [<output_file>]"
        exit(1)
    input_file = open(sys.argv[1], 'r')
    if len(sys.argv) > 2:
        output_file = open(sys.argv[2], 'w')
    else:
        output_file = sys.stdout

    # главный цикл
    for line in input_file:
        args = line[:-1].split('\t')
        handler = event_handlers[args[2]]
        handler(*args)

    # подготовка полученных данных
    request_times = array('I', sorted(request_times))
    backends = backends_map.values()
    backends.sort(key=lambda i: int(i.group))
    if len(slowlest_send_heap) == SLOWLEST_SEND_HEAP_SIZE:
        heapq.heappop(slowlest_send_heap)
    slowlest_send_iter = reversed([heapq.heappop(slowlest_send_heap) 
                                    for _ in xrange(len(slowlest_send_heap))])

    # вывод результата
    print >> output_file, "95 перцентиль времени работы: ", percentile(request_times, 0.95)
    print >> output_file, "Идентификаторы запросов с самой долгой фазой отправки данных пользователю:" 
    for t in slowlest_send_iter:
        print >> output_file,"\t", t[1]
    print >> output_file, "Запросов с неполным набором ответивших ГР: ", partial_merge_count
    print >> output_file, "Обращения и ошибки по бекендам:"
    for g, be_iter in groupby(backends, key=lambda b: b.group):
        print >> output_file, "ГР %s:" % g
        for be in be_iter:
            print >> output_file, "\n\t", be.id[7:-8]
            print >> output_file, "\t\tОбращения: ", be.requests
            if be.errors:
                print >> output_file, "\t\tОшибки: "
                for err in be.errors.iteritems():
                    print >> output_file, "\t\t\t%s: %s" % err
