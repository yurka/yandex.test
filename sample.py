# coding: utf-8

"""
Заливатор. Главный модуль.
"""

import re
import logging
import copy
from StringIO import StringIO

from django.conf import settings
from gorynych.shared.direct_api import DirectError, ScoresError
from gorynych.shared.xls_writer import XLSWriter

from . import models

logger = logging.getLogger(__name__)


class Uploader(object):

    """
    Заливатор.
        >>> uploader = Uploader(api)
        >>> try:
        >>>     report = uploader.upload(data_file)
        >>>     handle_report(report)
        >>> except ScoresError:
        >>>     retry_upload_task_tomorrow()

    """
    DEFAULT_CAMPAIGN_PARAMS = {
        'CampaignID': 0,
        'Strategy': {'StrategyName': 'HighestPosition'},
        'EmailNotification': {
            'WarnPlaceInterval': 60,
            'MoneyWarningValue': 20}
    }

    MAX_BANNERS_IN_SERIES = 300

    def __init__(self, api):
        self.api = api

    def upload(self, data_file, options):
        """
        Заливает один дата-файл.

            data_file: экземпляр models.DataFile
            options: словарь с опциями:
                {
                    'campaign_id': xxx,
                    'name': 'TheName'
                }

        Бросает ScoresError, если закончились баллы
        """
        report = UploaderReport(data_file)
        campaign_id = options['campaign_id']
        if campaign_id is None:
            campaign_id = self.create_campaign(data_file, options)
        report.campaigns.append(campaign_id)
        while _has_items_to_upload(data_file):
            capacity = self.get_capacity(campaign_id)
            if capacity == 0:
                campaign_id = self.create_campaign(data_file, options)
                report.campaigns.append(campaign_id)
                capacity = settings.GROUPS_MAX
            amount = min(capacity, self.MAX_BANNERS_IN_SERIES)
            uploaded_cnt, errors_count = self.upload_items(data_file,
                                                           campaign_id, amount)
            report.uploaded_banners_count += uploaded_cnt
            report.errors_count += errors_count
        report.status = report.STATUS_COMPLETED
        return report

    def create_campaign(self, data_file, options):
        """
        Создать кампанию для заливки
        """
        login = data_file.transfer.client.direct_login
        client_info = self.api.GetClientInfo([login])[0]
        name = self.make_campaign_name(options["name"])
        params = copy.deepcopy(self.DEFAULT_CAMPAIGN_PARAMS)
        params["Login"] = login
        params["Name"] = name
        params["FIO"] = client_info["FIO"]
        params['EmailNotification']['Email'] = client_info['Email']
        if data_file.minus_keywords:
            params.update(MinusKeywords=data_file.minus_keywords)
        campaign_id = self.api.CreateOrUpdateCampaign(params)
        logger.info("Campaign with Name=%s and "
                    "CampaignID=%d was just created", name, campaign_id)
        return campaign_id

    def get_capacity(self, campaign_id):
        """
        Определить свободное место в кампании
        """
        groups_set = {b['AdGroupID'] for b in
                      self.api.GetBanners({'CampaignID': campaign_id})}
        return settings.GROUPS_MAX - len(groups_set)

    def upload_items(self, data_file, campaign_id, amount):
        """
        Залить баннеры в указанную кампанию
        """
        items_to_upload = data_file.get_unprocessed_items(amount)
        return self._upload_chunk_banners(items_to_upload, campaign_id)

    def make_campaign_name(self, name):
        """
        Находит подходящее имя кампании
        """
        names = [c['Name'] for c in self.api.GetCampaignsList()]
        if name not in names:
            return name
        num = 1
        while name in names:
            name_nums = re.findall(r".*_(\d+)$", name)
            if name_nums:
                name = name[:-(len(name_nums[0]) + 1)] + '_%s' % num
            else:
                name = name + '_%s' % num
            num += 1
        return name

    def _upload_chunk_banners(self, banners_params, campaign_id):
        """
        Заливает или апдейтит баннеры. Если приходит ошибка,
        то информацию по баннеру кладет в self._bad_banners,
        остальные заливает
        """
        all_ids = []  # TODO: где использовать айдишники?

        banners_chunks = [banners_params]
        errors_count = 0
        while banners_chunks:
            chunk = banners_chunks.pop()
            try:
                banner_ids = self.api.CreateOrUpdateBanners(
                    [_prepare_banner(b, campaign_id) for b in chunk],
                    no_error_email=True)
            except ScoresError:
                raise
            except DirectError, err:
                if len(chunk) == 1:
                    logger.info("Direct error occured: %s", err.message)
                    # Если загружался один баннер и случилась ошибка,
                    # то это плохой баннер
                    bad_banner = chunk[0]
                    bad_banner.is_uploaded = True
                    bad_banner.reason = "{}: {}; {}".format(err.code,
                                                            err.message,
                                                            err.detail)
                    bad_banner.save()
                    errors_count += 1
                else:
                    banners_chunks.append(chunk[: len(chunk) / 2])
                    if chunk[len(chunk) / 2:]:
                        banners_chunks.append(chunk[len(chunk) / 2:])
            else:
                all_ids.extend(banner_ids)
                (models.BannerInfo.objects
                 .filter(pk__in=[b.pk for b in chunk])
                 .update(is_uploaded=True))
        return len(all_ids), errors_count


def _has_items_to_upload(data_file):
    """
    Выяснить, есть ли еще баннеры для заливки
    """
    return data_file.has_unprocessed_items()

def _prepare_banner(banner_info, campaign_id):
    """
    Подготовить данные баннера для заливки
    """
    data = banner_info.full_banner_info
    if data['CampaignID'] and data['CampaignID'] != campaign_id:
        raise RuntimeError(("Trying to upload banner with CampaignID={} "
                            "into campaign #{}").format(data['CampaignID'],
                                                        campaign_id))
    data["CampaignID"] = campaign_id
    return data


class UploaderReport(object):
    """
    Отчет о заливке
    """

    STATUS_COMPLETED = 'completed'
    STATUS_UNCOMPLETED = 'uncompleted'

    def __init__(self, data_file):
        self.status = self.STATUS_UNCOMPLETED
        self.data_file = data_file
        self.uploaded_banners_count = 0
        self.errors_count = 0
        self.campaigns = []

    @property
    def bad_banners(self):
        """Плохие баннеры - баннеры, которые не принял директ"""
        return (models.BannerInfo.objects
                .filter(datafile=self.data_file,
                        is_uploaded=True)
                .exclude(reason=""))

    def get_summary(self):
        """
        Возвращает строку с краткой сводкой по отчету
        """
        summary = (u"""Файл "{file}": залито {success}, с ошибками {errors},"""
                   u""" кампании {campaigns}""")
        return summary.format(
            file=self.data_file.filename,
            success=self.uploaded_banners_count,
            errors=self.errors_count,
            campaigns=','.join([" #%s" % c for c in self.campaigns]))

    def get_xls(self):
        """
        Формирует xls-файл с плохими баннерами
        """
        writer = XLSWriter()
        writer.add_sheet(u'Тексты')
        for _ in xrange(5):
            writer.add_row([])
        writer.add_row([
            u'Предложение текстовых блоков для рекламной кампании'])
        writer.add_row(['', '', '', u'№ заказа:', '', '', u'Валюта:'])
        minus_keywords = ','.join(self.data_file.minus_keywords)
        writer.add_row([
            '', '', '', u'Минус-слова на кампанию:', minus_keywords])
        for banner in self.bad_banners:
            row = banner.original_rows[0] + [banner.reason]
            writer.add_row(row)
            for row in banner.original_rows[1:]:
                writer.add_row(row)
        return StringIO(writer.write())
