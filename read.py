import sys

if __name__ == "__main__":
    input_file = open(sys.argv[1], 'r')
    output_file = open(sys.argv[2], 'w')
    cnt = 0
    for line in input_file:
        args = line.split('\t')
        cnt += 1
    print cnt, " lines"