import unittest

from parse2 import (
        Parser, 
        Request, 
        Backend, 
        MaxQueue,
        percentile
    ) 


class TestMaxQueue(unittest.TestCase):

    def test_len(self):
        q = MaxQueue(2)
        self.assertEqual(len(q), 0)
        q._heap = [1]
        self.assertEqual(len(q), 1)
        q._heap = [1, 2]
        self.assertEqual(len(q), 2)

    def test_push(self):
        q = MaxQueue(2)
        q.push(3)
        q.push(2)
        q.push(1)
        self.assertEqual(len(q), 2)
        self.assertEqual(q._heap, [2, 3])

    def test_pop(self):
        q = MaxQueue(2)
        q.push(3)
        q.push(2)
        q.push(1)
        self.assertEqual(q.pop(), 2)
        self.assertEqual(q.pop(), 3)
        q.push(3)
        q.push(2)
        q.push(1)
        q.push(4)
        self.assertEqual(q.pop(), 3)
        self.assertEqual(q.pop(), 4)
        self.assertRaises(IndexError, q.pop)

    def test_iter(self):
        q = MaxQueue(3)
        q.push(2)
        q.push(5)
        q.push(1)
        q.push(4)
        self.assertEqual(list(q), [2,4,5])


class ParserTest(unittest.TestCase):

    def test_start_request(self):
        log = ["100\t1\tStartRequest\n"]
        p = Parser()
        p.parse(log)
        self.assertEqual(len(p.request_map), 1)
        req = p.request_map.get("1")
        self.assertEqual(req.id, "1")
        self.assertEqual(req.start_request_time, 100)

    def test_be_connect(self):
        log = ["100\t1\tBackendConnect\t0\turl\n"]
        p = Parser()
        req = Request('1', 100)
        p.request_map['1'] = req
        p.parse(log)
        self.assertEqual(len(p.backends_map), 1)
        be = p.backends_map.get('url')
        self.assertEqual(be.id, 'url')
        self.assertEqual(be.group, '0')
        self.assertEqual(be.errors, {})
        self.assertEqual(be.requests, 1)
        self.assertEqual(req.backends.get('0'), be)
        self.assertEqual(req.backend_requests.get('0'), 1)

    def test_be_ok(self):
        log = ["100\t1\tBackendOk\t0\n"]
        p = Parser()
        req = Request('1', 100)
        p.request_map['1'] = req
        req.backend_requests['0'] = 1
        p.parse(log)
        self.assertEqual(req.backend_requests.get('0'), 0)

    def test_be_error(self):
        log1 = ["100\t3\tBackendError\t3\tSome Error\n"]
        log2 = ["100\t3\tBackendError\t3\tOther Error\n"]
        p = Parser()
        req = Request('3', 100)
        be = Backend('3', 'url')
        req.backends['3'] = be
        p.request_map['3'] = req
        p.parse(log1)
        self.assertEqual(be.errors.get('Some Error'), 1)
        self.assertEqual(be.errors.get('Other Error'), None)
        p.parse(log2)
        self.assertEqual(be.errors.get('Some Error'), 1)
        self.assertEqual(be.errors.get('Other Error'), 1)
        p.parse(log1)
        self.assertEqual(be.errors.get('Some Error'), 2)
        self.assertEqual(be.errors.get('Other Error'), 1)

    def test_send_result(self):
        log = ["200\t1\tStartSendResult\n"]
        p = Parser()
        req = Request('1', 100)
        p.request_map['1'] = req
        p.parse(log)
        self.assertEqual(req.start_send_time, 200)

    def test_finish(self):
        log = ["300\t1\tFinishRequest\n"]
        p = Parser()
        req = Request('1', 100)
        req.start_send_time = 250
        p.request_map['1'] = req
        p.parse(log)
        self.assertEqual(p.request_map, {})
        self.assertEqual(list(p.request_times), [200])
        self.assertEqual(list(p.slowlest_send), [(50, '1')])

    def test_parse(self):
        log = [
            "100\t1\tStartRequest\n",
            "200\t1\tBackendConnect\t0\turl-0.0\n",
            "300\t1\tBackendConnect\t1\turl-1.0\n",
            "400\t1\tBackendError\t1\tConnect Error\n",
            "500\t1\tBackendRequest\t0\n",
            "600\t1\tBackendConnect\t1\turl-1.1\n",
            "700\t1\tBackendError\t0\tFatal Error\n",
            "800\t1\tBackendConnect\t0\turl-0.1\n",
            "900\t1\tBackendRequest\t1\n",
            "1000\t1\tBackendRequest\t0\n",
            "1100\t1\tBackendOk\t0\n",
            "1200\t1\tBackendOk\t1\n",
            "1300\t1\tStartMerge\n",
            "1400\t1\tStartSendResult\n",
            "1500\t1\tFinishRequest\n",
        ]
        p = Parser()
        p.parse(log)

        self.assertEqual(p.request_map, {})
        self.assertEqual(list(p.request_times), [1500 - 100])
        self.assertEqual(list(p.slowlest_send), [(1500 - 1400, '1')])
        self.assertEqual(p.partial_merge_count, 0)
        self.assertEqual(sorted(p.backends_map.keys()), ['url-0.0', 'url-0.1', 'url-1.0', 'url-1.1'])

        self.assertEqual(p.backends_map['url-0.0'].group, '0')
        self.assertEqual(p.backends_map['url-0.0'].id, 'url-0.0')
        self.assertEqual(p.backends_map['url-0.0'].requests, 1)
        self.assertEqual(p.backends_map['url-0.0'].errors, {"Fatal Error": 1})

        self.assertEqual(p.backends_map['url-0.1'].group, '0')
        self.assertEqual(p.backends_map['url-0.1'].id, 'url-0.1')
        self.assertEqual(p.backends_map['url-0.1'].requests, 1)
        self.assertEqual(p.backends_map['url-0.1'].errors, {})

        self.assertEqual(p.backends_map['url-1.0'].group, '1')
        self.assertEqual(p.backends_map['url-1.0'].id, 'url-1.0')
        self.assertEqual(p.backends_map['url-1.0'].requests, 1)
        self.assertEqual(p.backends_map['url-1.0'].errors, {"Connect Error": 1})

        self.assertEqual(p.backends_map['url-1.1'].group, '1')
        self.assertEqual(p.backends_map['url-1.1'].id, 'url-1.1')
        self.assertEqual(p.backends_map['url-1.1'].requests, 1)
        self.assertEqual(p.backends_map['url-1.1'].errors, {})

        log2 = log + [
            "110\t2\tStartRequest\n",
            "210\t2\tBackendConnect\t2\turl-2.0\n",
            "310\t2\tBackendConnect\t1\turl-1.0\n",
            "410\t2\tBackendError\t1\tConnect Error\n",
            "510\t2\tBackendRequest\t2\n",
            "610\t2\tBackendConnect\t1\turl-1.1\n",
            "710\t2\tBackendError\t2\tFatal Error\n",
            "810\t2\tBackendConnect\t2\turl-2.1\n",
            "910\t2\tBackendRequest\t1\n",
            "1010\t2\tBackendRequest\t2\n",
            "1210\t2\tBackendOk\t1\n",
            "1310\t2\tStartMerge\n",
            "1410\t2\tStartSendResult\n",
            "1600\t2\tFinishRequest\n",
        ]

        p = Parser()
        p.parse(log2)
        self.assertEqual(p.request_map, {})
        self.assertEqual(sorted(p.request_times), [1500 - 100, 1600 - 110])
        self.assertEqual(list(p.slowlest_send), [(1500 - 1400, '1'), (1600 - 1410, '2')])
        self.assertEqual(p.partial_merge_count, 1)
        self.assertEqual(sorted(p.backends_map.keys()), ['url-0.0', 'url-0.1', 'url-1.0', 
            'url-1.1', 'url-2.0', 'url-2.1'])

        for i in xrange(5):
            p.parse(log2)
        self.assertEqual(p.partial_merge_count, 6)
        self.assertEqual(len(p.slowlest_send), 10)
        self.assertEqual(p.backends_map['url-2.0'].requests, 6)
        self.assertEqual(p.backends_map['url-2.0'].errors, {'Fatal Error': 6})


    def test_percentile(self):
        self.assertEqual(percentile([], 0.95), None)
        self.assertEqual(percentile(range(101), 0.5), 50)
        self.assertEqual(percentile(range(101), 0.95), 95)
        self.assertEqual(percentile(range(11), 0.75), 7.5)
        self.assertEqual(percentile(range(2), 0.75), 0.75)
        self.assertEqual(percentile(range(2), 0.5), 0.5)
        self.assertRaises(ValueError, percentile, [0, 1], 1.5)


if __name__ == '__main__':
    unittest.main()
