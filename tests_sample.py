
class BannerFactory(factory.DictFactory):

    BannerID = factory.Sequence(lambda n: 1000 + n)
    AdGroupID = factory.Sequence(lambda n: 1000 + n)
    CampaignID = 0
    Title = factory.Sequence(lambda n: u"Заголовок %s" % n)
    Text = factory.Sequence(lambda n: u"Текст %s" % n)


class CampaignFactory(factory.DictFactory):

    CampaignID = factory.Sequence(lambda n: 100 + n)
    Name = factory.Sequence(lambda n: u"Кампания %s" % n)


class UserFactory(factory.DjangoModelFactory):

    class Meta:
        model = 'auth.User'
    username = factory.Sequence(lambda n: u"user_%s" % n)


class ClientFactory(factory.DjangoModelFactory):

    class Meta:
        model = 'shared.Client'
    direct_login = factory.Sequence(lambda n: u"client_%s" % n)


class TransferFactory(factory.DjangoModelFactory):

    class Meta:
        model = 'uploader.Transfer'

    client = factory.SubFactory(ClientFactory)
    manager = factory.SubFactory(UserFactory)
    upload_start_time = factory.LazyAttribute(lambda o: datetime.now())
    upload_finish_time = factory.LazyAttribute(lambda o: datetime.now())


class DataFileFactory(factory.DjangoModelFactory):

    class Meta:
        model = 'uploader.DataFile'

    transfer = factory.SubFactory(TransferFactory)
    filename = factory.Sequence(lambda n: u"File_%s" % n)


class BannerInfoFactory(factory.DjangoModelFactory):

    class Meta:
        model = 'uploader.BannerInfo'

    datafile = factory.SubFactory(DataFileFactory)
    full_banner_info = factory.SubFactory(BannerFactory)
    seq_number = factory.Sequence(lambda n: n)
    original_rows = factory.Sequence(lambda n: [[n, 'c1_%s' % n, 'c2_%s' % n]])


def reset_factories():
    BannerFactory.reset_sequence(force=True)
    CampaignFactory.reset_sequence(force=True)
    UserFactory.reset_sequence(force=True)
    ClientFactory.reset_sequence(force=True)
    TransferFactory.reset_sequence(force=True)
    DataFileFactory.reset_sequence(force=True)
    BannerInfoFactory.reset_sequence(force=True)


class TestUploader(test.TestCase):

    def setUp(self):
        self.api = Mock()
        self.upl = main.Uploader(self.api)
        reset_factories()

    def test_get_capacity(self):
        self.api.GetBanners.return_value = BannerFactory.build_batch(
            1000, CampaignID=123)
        assert self.upl.get_capacity(123) == 0
        self.api.GetBanners.assert_called_once_with({'CampaignID': 123})

        self.api.reset_mock()
        BannerFactory.reset_sequence(force=True)
        self.api.GetBanners.return_value = BannerFactory.build_batch(
            950, CampaignID=123)
        assert self.upl.get_capacity(123) == 50
        self.api.GetBanners.assert_called_once_with({'CampaignID': 123})

        self.api.reset_mock()
        BannerFactory.reset_sequence(force=True)
        self.api.GetBanners.return_value = (
            BannerFactory.build_batch(990, CampaignID=123)
            + BannerFactory.build_batch(10, CampaignID=123, AdGroupID=1989)
            + BannerFactory.build_batch(10, CampaignID=123, AdGroupID=1001))

        assert self.upl.get_capacity(123) == 10
        self.api.GetBanners.assert_called_once_with({'CampaignID': 123})

    def test_create_campaign(self):
        self.api.GetCampaignsList.return_value = CampaignFactory.build_batch(5)
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]
        self.api.CreateOrUpdateCampaign.return_value = 321

        df = DataFileFactory.build()
        opts = {'name': u'Кампания Х'}
        cid = self.upl.create_campaign(df, opts)

        assert cid == 321
        assert self.api.GetCampaignsList.called
        self.api.CreateOrUpdateCampaign.assert_called_once_with(
            {'Login': 'client_1',
             'CampaignID': 0,
             'Name': u'Кампания Х',
             'FIO': 'Client Fio',
             'Strategy': {'StrategyName': 'HighestPosition'},
             'EmailNotification': {
                 'Email': 'email.of@client',
                 'WarnPlaceInterval': 60,
                 'MoneyWarningValue': 20}
             })

    def test_create_campaign_name_exist(self):
        self.api.GetCampaignsList.return_value = (
            CampaignFactory.build_batch(5)
            + [CampaignFactory.build(Name=u"Кампания Y")])
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]
        self.api.CreateOrUpdateCampaign.return_value = 321

        df = DataFileFactory.build(minus_keywords=[u'разные', u'слова'])
        opts = {'name': u'Кампания Y'}
        cid = self.upl.create_campaign(df, opts)

        assert cid == 321
        assert self.api.GetCampaignsList.called
        self.api.CreateOrUpdateCampaign.assert_called_once_with(
            {'Login': 'client_1',
             'CampaignID': 0,
             'Name': u'Кампания Y_1',
             'FIO': 'Client Fio',
             'Strategy': {'StrategyName': 'HighestPosition'},
             'EmailNotification': {
                 'Email': 'email.of@client',
                 'WarnPlaceInterval': 60,
                 'MoneyWarningValue': 20},
             'MinusKeywords': [u'разные', u'слова']
             })

    def test_make_campaign_name(self):
        self.api.GetCampaignsList.return_value = [
            CampaignFactory.build(Name=u"Кампания"),
            CampaignFactory.build(Name=u"Кампания X"),
            CampaignFactory.build(Name=u"Кампания X_1"),
            CampaignFactory.build(Name=u"Кампания X_2"),
            CampaignFactory.build(Name=u"Кампания Y"),
            CampaignFactory.build(Name=u"Кампания Y_2"),
            CampaignFactory.build(Name=u"Кампания Y_3"),
            CampaignFactory.build(Name=u"Кампания Z"),
            CampaignFactory.build(Name=u"Кампания Z_1"),
            CampaignFactory.build(Name=u"Кампания Z_3"),
        ]

        assert self.upl.make_campaign_name(u"Кампания") == u"Кампания_1"
        assert self.upl.make_campaign_name(u"Кампания X") == u"Кампания X_3"
        assert self.upl.make_campaign_name(u"Кампания X5") == u"Кампания X5"
        assert self.upl.make_campaign_name(u"Кампания Y") == u"Кампания Y_1"
        assert self.upl.make_campaign_name(u"Кампания Z") == u"Кампания Z_2"

    def test_upload_new_campaign_all_ok(self):
        self.api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        BannerFactory.reset_sequence(force=True)
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]
        self.api.CreateOrUpdateCampaign.return_value = 444
        self.api.CreateOrUpdateBanners.return_value = [11, 12]
        self.api.GetBanners.return_value = []

        df = DataFileFactory()
        BannerInfoFactory.create_batch(2, datafile=df)
        opts = {'campaign_id': None, 'name': u'Новая Кампания'}
        upl = main.Uploader(self.api)
        upl.upload(df, opts)

        expected = [
            {"BannerID": 1000,
             "CampaignID": 444,
             "AdGroupID": 1000,
             "Text": u"Текст 0",
             "Title": u"Заголовок 0"
             },

            {"BannerID": 1001,
             "CampaignID": 444,
             "AdGroupID": 1001,
             "Text": u"Текст 1",
             "Title": u"Заголовок 1"
             },
        ]
        self.api.CreateOrUpdateBanners.assert_called_once_with(
            expected,
            no_error_email=True)

    def test_upload_old_and_new_campaign_all_ok(self):
        self.api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        self.api.CreateOrUpdateCampaign.return_value = 444
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]

        self.api.GetBanners.side_effect = [BannerFactory.build_batch(998),
                                           BannerFactory.build_batch(1000)]
        self.api.CreateOrUpdateBanners.side_effect = [[11, 12], [13, 14]]

        df = DataFileFactory()
        BannerFactory.reset_sequence(force=True)
        BannerInfoFactory.create_batch(4, datafile=df)
        opts = {'campaign_id': 101, 'name': u'Кампания 1'}
        upl = main.Uploader(self.api)
        report = upl.upload(df, opts)

        expected = [
            {"BannerID": 1000,
             "CampaignID": 101,
             "AdGroupID": 1000,
             "Text": u"Текст 0",
             "Title": u"Заголовок 0"
             },

            {"BannerID": 1001,
             "CampaignID": 101,
             "AdGroupID": 1001,
             "Text": u"Текст 1",
             "Title": u"Заголовок 1"
             },
        ]

        expected_2 = [
            {"BannerID": 1002,
             "CampaignID": 444,
             "AdGroupID": 1002,
             "Text": u"Текст 2",
             "Title": u"Заголовок 2"
             },

            {"BannerID": 1003,
             "CampaignID": 444,
             "AdGroupID": 1003,
             "Text": u"Текст 3",
             "Title": u"Заголовок 3"
             },
        ]
        assert self.api.CreateOrUpdateCampaign.call_count == 1
        assert self.api.CreateOrUpdateBanners.call_count == 2
        assert (self.api.CreateOrUpdateBanners.call_args_list ==
                [call(expected, no_error_email=True),
                 call(expected_2, no_error_email=True)])

        assert report.errors_count == 0
        assert report.uploaded_banners_count == 4
        assert len(report.bad_banners) == 0
        assert report.campaigns == [101, 444]

    def test_upload_new_campaign_bad_banner(self):
        self.api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        BannerFactory.reset_sequence(force=True)
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]
        self.api.CreateOrUpdateCampaign.return_value = 444
        self.api.CreateOrUpdateBanners.side_effect = [
            DirectError('A', 1, 'msg', 'Bad banner 1'),
            DirectError('B', 2, 'msg', 'Bad banner 2'),
            DirectError('С', 3, 'msg', 'Bad banner 3'),
        ]
        self.api.GetBanners.return_value = []

        df = DataFileFactory()
        BannerInfoFactory.create_batch(2, datafile=df)
        opts = {'campaign_id': None, 'name': u'Новая Кампания'}
        report = self.upl.upload(df, opts)

        expected = [
            {"BannerID": 1000,
             "CampaignID": 100,
             "AdGroupID": 1000,
             "Text": u"Текст 0",
             "Title": u"Заголовок 0"
             },

            {"BannerID": 1001,
             "CampaignID": 101,
             "AdGroupID": 1001,
             "Text": u"Текст 1",
             "Title": u"Заголовок 1"
             },
        ]
        assert (self.api.CreateOrUpdateBanners.call_args_list ==
                [call(expected, no_error_email=True),
                 call(expected[0], no_error_email=True),
                 call(expected[1], no_error_email=True)],
                )
        bad_banners = models.BannerInfo.objects.order_by('pk').all()
        assert len(bad_banners) == 2
        assert bad_banners[0].is_uploaded is True
        assert bad_banners[0].reason == '3: msg; Bad banner 3'
        assert bad_banners[1].is_uploaded is True
        assert bad_banners[1].reason == '2: msg; Bad banner 2'

        assert report.errors_count == 2
        assert report.uploaded_banners_count == 0
        assert len(report.bad_banners) == 2
        assert report.campaigns == [444]

    def test_upload_no_scores(self):
        self.api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        self.api.CreateOrUpdateCampaign.return_value = 444
        self.api.GetClientInfo.return_value = [{'FIO': 'Client Fio',
                                                'Email': 'email.of@client'}]

        self.api.GetBanners.side_effect = [BannerFactory.build_batch(998),
                                           BannerFactory.build_batch(1000)]
        self.api.CreateOrUpdateBanners.side_effect = [
            [11, 12],
            ScoresError('A', 1, 'need more minerals', 'Bad banner 1')]

        df = DataFileFactory()
        BannerFactory.reset_sequence(force=True)
        BannerInfoFactory.create_batch(4, datafile=df)
        opts = {'campaign_id': 101, 'name': u'Кампания 1'}
        upl = main.Uploader(self.api)
        with self.assertRaises(ScoresError):
            upl.upload(df, opts)
        banners = models.BannerInfo.objects.order_by('pk').all()
        assert len(banners) == 4
        assert banners[0].is_uploaded is True
        assert banners[1].is_uploaded is True
        assert banners[2].is_uploaded is False
        assert banners[3].is_uploaded is False


class ReportTest(test.TestCase):

    def setUp(self):
        reset_factories()

    def test_get_summary(self):
        df = DataFileFactory()
        banners = BannerInfoFactory.create_batch(10, datafile=df)
        report = main.UploaderReport(df)
        report.uploaded_banners_count = 10
        report.campaigns = [122, 233]

        expected = (u"""Файл "File_1": залито 10, с ошибками 0,"""
                    u""" кампании  #122, #233""")
        assert report.get_summary() == expected

    def test_get_xls(self):
        # smoke test
        f = open('report.xls', 'w')
        df = DataFileFactory(minus_keywords=['aa', 'bb'])
        banners = BannerInfoFactory.create_batch(10, datafile=df,
                                                 reason='bug', is_uploaded=True)
        report = main.UploaderReport(df)
        report.uploaded_banners_count = 10
        report.campaigns = [122, 233]
        f.write(report.get_xls().read())
        f.close()


class TasksTest(test.TestCase):

    def setUp(self):
        reset_factories()

    @patch('gorynych.shared.direct_api.DirectApi')
    def test_upload_data_file(self, mock_api_cls):
        api = Mock()
        mock_api_cls.return_value = api

        api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        BannerFactory.reset_sequence(force=True)
        api.CreateOrUpdateBanners.return_value = [1, 2, 3]
        api.GetBanners.return_value = []
        dfile = DataFileFactory.create()
        BannerInfoFactory.create_batch(3, datafile=dfile)
        report = tasks.upload_data_file(dfile.pk, {'campaign_id': 102})

        assert report.uploaded_banners_count == 3

    @patch('gorynych.shared.direct_api.DirectApi')
    def test_process_transfer(self, mock_api_cls):
        api = Mock()
        mock_api_cls.return_value = api

        api.GetCampaignsList.return_value = CampaignFactory.build_batch(3)
        BannerFactory.reset_sequence(force=True)
        api.CreateOrUpdateBanners.side_effect = [
            [1, 2, 3, 4, 5],
            DirectError('A', 1, 'msg', 'Bad banner 1'),
            [1, 2],
            DirectError('A', 1, 'msg', 'Bad banner 1'),
            [1, 2, 3, 4, 5, 6, 7],
        ]
        api.GetBanners.return_value = []

        t = TransferFactory()
        dfiles = DataFileFactory.create_batch(3, transfer=t)
        BannerInfoFactory.create_batch(5, datafile=dfiles[0])
        BannerInfoFactory.create_batch(3, datafile=dfiles[1])
        BannerInfoFactory.create_batch(7, datafile=dfiles[2])

        tasks.process_transfer(
            t.pk,
            [
                {'data_file_id': dfiles[0].pk,
                 'campaign_id': 100},
                {'data_file_id': dfiles[1].pk,
                 'campaign_id': 101},
                {'data_file_id': dfiles[2].pk,
                 'campaign_id': 102},
            ])

        self.assertEqual(len(mail.outbox), 1)
        self.assertIn(u'Заливатор для client_1', mail.outbox[0].subject)
        self.assertIn(u'Файл "File_1": залито 5, с ошибками 0, кампании  #100',
                      mail.outbox[0].body)
        self.assertIn(u'Файл "File_2": залито 2, с ошибками 1, кампании  #101',
                      mail.outbox[0].body)
        self.assertIn(u'Файл "File_3": залито 7, с ошибками 0, кампании  #102',
                      mail.outbox[0].body)
        self.assertEqual(len(mail.outbox[0].attachments), 1)

    @patch('uploader.tasks.upload_data_file')
    def test_process_transfer_reshedule(self, mock_udf):
        tasks.process_transfer.apply_async = Mock()
        mock_udf.side_effect =  ScoresError('A', 1, 'need more minerals', 
                                            'Bad banner 1')


        t = TransferFactory()
        dfiles = DataFileFactory.create_batch(3, transfer=t)
        BannerInfoFactory.create_batch(5, datafile=dfiles[0])
        BannerInfoFactory.create_batch(3, datafile=dfiles[1])
        BannerInfoFactory.create_batch(7, datafile=dfiles[2])
        opts = [
                {'data_file_id': dfiles[0].pk,
                 'campaign_id': 100},
                {'data_file_id': dfiles[1].pk,
                 'campaign_id': 101},
                {'data_file_id': dfiles[2].pk,
                 'campaign_id': 102},
            ]

        tasks.process_transfer(t.pk, opts)
        t = models.Transfer.objects.get(pk=t.pk)
        self.assertEqual(t.status, models.STATUS_DELAYED)
        self.assertEqual(tasks.process_transfer.apply_async.call_count, 1)
