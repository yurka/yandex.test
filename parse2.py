# coding: utf-8

"""
    решение тестового задания http://yadi.sk/d/gPd_7q5sGutPx
    парсер событийного лога

    $ python parse2.py <input_file> [<output_file>]

    если output_file не задан - вывод осуществляется в stdout
"""

import sys
import math
import heapq
from array import array
from itertools import groupby


START_REQUEST = 'StartRequest'
BACKEND_CONNECT = 'BackendConnect'
BACKEND_REQUEST = 'BackendRequest'
BACKEND_OK = 'BackendOk'
BACKEND_ERROR = 'BackendError'
START_MERGE = 'StartMerge'
START_SEND_RESULT = 'StartSendResult'
FINISH_REQUEST = 'FinishRequest'

# Cколько запросов, для которых фаза отправки результатов пользователю 
# была максимальной, нам нужно отображать
SLOWLEST_SEND_NUMBER = 10 


class Request(object):
    """
    Запрос.

        id - уникальный идентификатор запроса
        start_request_time - метка времени начала обработки запроса
        backends - словарь, хранящий ссылки на бекенды, задействованные в данном
                   запросе по их номеру ГР
        backend_requests - словарь, хранящий флаги обращения к бекендам по номеру ГР.
                           обратились к бекенду - ставим флаг, получили ответ от него 
                           - снимаем флаг. Если на момент завершения запроса в словаре
                           есть неснятые флаги - значит у нас получены данные 
                           не от всех ГР (неполный мердж)

    """
    
    def __init__(self, rid, start_time):
        self.id = rid
        self.start_request_time = start_time
        self.backends = {}
        self.backend_requests = {}


class Backend(object):
    """
    Бекенд.

        id - уникальный идентификатор бекенда
        group - группа реплик, к которой относится бекенд
        errors - словарь {'Тип ошибки': количество ошибок} - счетчик ошибок по типам
        requests - количество обращений к бекенду
    """

    __slots__ = ("group", "id", "errors", "requests")

    def __init__(self, group, url):
        self.group = group
        self.id = url
        self.errors = {}
        self.requests = 0


class MaxQueue(object):
    """
    Очередь, накапливающая элементы с максимальным значением. 
    После заполнения очереди, при дальнейшем добавлении новых элементов,
    элементы с минимальными значениями удаляются из очереди и она сохраняет заданный размер

        size - рзмер очереди
    """

    def __init__(self, size):
        self.size = size
        self._heap = []

    def push(self, value):
        """
        Добавляет новый элемент в очередь. 
        Если очередь заполнена, из нее удаляется минимальный элемент
        и ее размер не изменяется 
        """
        if len(self) < self.size:
            heapq.heappush(self._heap, value)
        else:
            heapq.heappushpop(self._heap, value)

    def pop(self):
        """
        Извлекает и возвращает минимальный элемент очереди
        """
        return heapq.heappop(self._heap)

    def __len__(self):
        return len(self._heap)

    def __iter__(self):
        """
        Возвращает итератор, отдающий элементы очереди в порядке возрастания
        """
        while self._heap:
            yield heapq.heappop(self._heap)




class Parser(object):
    """
    Парсер логов.

        request_map - словарь для хранения объектов запросов 
        backends_map - словарь для хранения объектов бекендов 
        request_times - целочисленный массив для величин времени запроса
        partial_merge_count - счетчик количества запросов, на которые фронтенд 
                              не смог собрать данные со всех ГР
        slowlest_send - список запросов, для которых фаза отправки результатов
                             пользователю была максимальной
        event_handlers - отображение "событие" -> "обработчик"
    """

    def __init__(self):
        self.request_map = {}
        self.backends_map = {}
        self.request_times = array('I')
        self.partial_merge_count = 0
        self.slowlest_send = MaxQueue(SLOWLEST_SEND_NUMBER)
        self.event_handlers = {
            START_REQUEST: self.handle_start_request,
            BACKEND_CONNECT: self.handle_be_connect,
            BACKEND_REQUEST: self.handle_be_request,
            BACKEND_OK: self.handle_be_ok,
            BACKEND_ERROR: self.handle_be_error,
            START_MERGE: self.handle_merge,
            START_SEND_RESULT: self.handle_send_result,
            FINISH_REQUEST: self.handle_finish
        }

    def parse(self, log):
        """
        Парсинг лога. 
        Параметр log - итератор, отдающий построчно данные лога.
        """
        for line in log:
            args = line[:-1].split('\t')
            handler = self.event_handlers[args[2]]
            handler(*args)

    def handle_start_request(self, timestamp, request_id, e):
        """
        Обработчик записи о событии START_REQUEST.
        """
        req = Request(request_id, int(timestamp))
        self.request_map[request_id] = req

    def handle_be_connect(self, timestamp, request_id, e, group, url):
        """
        Обработчик записи о событии BACKEND_CONNECT.
        """
        req = self.request_map[request_id]
        backend = self.backends_map.get(url)
        if not backend:
            backend = Backend(group, url)
            self.backends_map[url] = backend
        # регистрируем обращение к бекенду
        backend.requests += 1
        # Ставим флаг, означающий что мы начали работу с бекендом из ГР "group"
        # в контексте запроса "request_id". Мы снимем флаг, когда получим ответ от этой ГР.
        # Если флаг не будет снят - это будет означать, что в данном запросе получены данные не от всех ГР. 
        req.backend_requests[group]  = 1
        req.backends[group] = backend

    def handle_be_request(self, timestamp, request_id, e, group):
        """
        Обработчик записи о событии BACKEND_REQUEST.
        """
        pass

    def handle_be_ok(self, timestamp, request_id, e, group):
        """
        Обработчик записи о событии BACKEND_OK.
        """
        req = self.request_map[request_id]
        # мы получили данные от бекенда ГР "group", снимаем флаг
        req.backend_requests[group] = 0

    def handle_be_error(self, timestamp, request_id, e, group, error_msg):
        """
        Обработчик записи о событии BACKEND_ERROR.
        """
        req = self.request_map[request_id]
        backend = req.backends[group]
        # регистрируем ошибку для бекенда
        backend.errors[error_msg] = backend.errors.get(error_msg, 0) + 1

    def handle_merge(self, timestamp, request_id, e):
        """
        Обработчик записи о событии START_MERGE.
        """
        pass

    def handle_send_result(self, timestamp, request_id, e):
        """
        Обработчик записи о событии START_SEND_RESULT.
        """
        req = self.request_map[request_id]
        # регистрируем время события
        req.start_send_time = int(timestamp)

    def handle_finish(self, timestamp, request_id, e):
        """
        Обработчик записи о событии FINISH_REQUEST.
        """
        req = self.request_map[request_id]
        # замеряем полное время обработки запроса
        # и время, которое заняла фаза отправки данных 
        total_time = int(timestamp) - req.start_request_time
        send_time = int(timestamp) - req.start_send_time
        # отмечаем неполный мердж
        if any(req.backend_requests.values()):
            self.partial_merge_count += 1
        self.request_times.append(total_time)
        self.slowlest_send.push((send_time, req.id))
        del self.request_map[request_id]



def percentile(values, percent):
    """
    Вычисление перцентиля.
        values - список величин, должен быть отсортирован по возрастанию
        percent - перцентиль, число от 0 до 1
    """
    if not values:
        return None
    if not 0 <= percent <= 1:
        raise ValueError("Invalid percent value, must be floating poin number in range [0,1]")
    k = (len(values)-1) * percent
    f = math.floor(k)
    c = math.ceil(k)
    if f == c:
        return values[int(k)]
    d0 = values[int(f)] * (c - k)
    d1 = values[int(c)] * (k - f)
    return d0 + d1


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "\tevent log parser"
        print "\tusage: python parse.py <input_file> [<output_file>]"
        exit(1)
    input_file = open(sys.argv[1], 'r')
    if len(sys.argv) > 2:
        output_file = open(sys.argv[2], 'w')
    else:
        output_file = sys.stdout

    # парсинг
    parser = Parser()
    parser.parse(input_file)

    # подготовка полученных данных
    request_times = array('I', sorted(parser.request_times))
    backends = parser.backends_map.values()
    backends.sort(key=lambda i: int(i.group))
    
    slowlest_send_iter = reversed(list(parser.slowlest_send))

    # вывод результата
    print >> output_file, "95 перцентиль времени работы: ", percentile(request_times, 0.95)
    print >> output_file, "Идентификаторы запросов с самой долгой фазой отправки данных пользователю:" 
    for t in slowlest_send_iter:
        print >> output_file,"\t", t[1]
    print >> output_file, "Запросов с неполным набором ответивших ГР: ", parser.partial_merge_count
    print >> output_file, "Обращения и ошибки по бекендам:"
    for g, be_iter in groupby(backends, key=lambda b: b.group):
        print >> output_file, "ГР %s:" % g
        for be in be_iter:
            print >> output_file, "\n\t", be.id[7:-8]
            print >> output_file, "\t\tОбращения: ", be.requests
            if be.errors:
                print >> output_file, "\t\tОшибки: "
                for err in be.errors.iteritems():
                    print >> output_file, "\t\t\t%s: %s" % err
