import csv
import sys

if __name__ == "__main__":
    input_file = open(sys.argv[1], 'r')
    output_file = open(sys.argv[2], 'w')
    cnt = 0
    rdr = csv.reader(input_file, delimiter='\t')
    for row in rdr:
        cnt += 1
    print cnt, " lines"

