import sys


START_REQUEST = intern('StartRequest')
BACKEND_CONNECT = intern('BackendConnect')
BACKEND_REQUEST = intern('BackendRequest')
BACKEND_OK = intern('BackendOk')
BACKEND_ERROR = intern('BackendError')
START_MERGE = intern('StartMerge')
START_SEND_RESULT = intern('StartSendResult')
FINISH_REQUEST = intern('FinishRequest')


class Request(object):

    __slots__ = ("start_request_time", "start_send_time", "id", "total_time", "failed_be_requests", "_backends", "send_time")

    def __init__(self, rid, start_time):
        self.id = rid
        self.start_request_time = start_time
        self.start_send_time = None
        self.total_time = None
        self.failed_be_requests = 0
        self._backends = {}

    def add_backend(self, be):
        if be.group in self._backends:
            self.failed_be_requests -= 1
        self._backends[be.group] = be

    def get_backend(self, group):
        return self._backends[int(group)]


class Backend(object):

    __slots__ = ("group", "id", "errors", "requests")

    def __init__(self, group, url):
        self.group = int(group)
        self.id = url[7:-8]
        self.errors = {}
        self.requests = 0


request_map = {}
request_times = []
send_times = []
backends_map = {}


# def handle_start_request(timestamp, request_id):
#     req = Request(request_id, int(timestamp))
#     request_map[request_id] = req


# def handle_be_connect(timestamp, request_id, group, url):
#     req = request_map[request_id]
#     backend = Backend(group, url)
#     req.add_backend(backend)
#     backends_map[backend.id] = backend


# def handle_be_request(timestamp, request_id, group):
#     req = request_map[request_id]
#     req.failed_be_requests += 1
#     backend = req.get_backend(group)
#     backend.requests += 1


# def handle_be_ok(timestamp, request_id, group):
#     req = request_map[request_id]
#     req.failed_be_requests -= 1
#     # req.remove_backend(backend)


# def handle_be_error(timestamp, request_id, group, error_msg):
#     req = request_map[request_id]
#     backend = req.get_backend(group)
#     backend.errors[error_msg] = backend.errors.get(error_msg, 0) + 1


# def handle_merge(timestamp, request_id):
#     pass


# def handle_send_result(timestamp, request_id):
#     req = request_map[request_id]
#     req.start_send_time = int(timestamp)


# def handle_finish(timestamp, request_id):
#     req = request_map[request_id]
#     req.total_time = int(timestamp) - req.start_request_time
#     req.send_time = int(timestamp) - req.start_send_time
#     request_times.append(req.total_time)
#     send_times.append(req.send_time)


# event_handlers = {
#     START_REQUEST: handle_start_request,
#     BACKEND_CONNECT: handle_be_connect,
#     BACKEND_REQUEST: handle_be_request,
#     BACKEND_OK: handle_be_ok,
#     BACKEND_ERROR: handle_be_error,
#     START_MERGE: handle_merge,
#     START_SEND_RESULT: handle_send_result,
#     FINISH_REQUEST: handle_finish
# }



if __name__ == "__main__":
    input_file = open(sys.argv[1], 'r')
    output_file = open(sys.argv[2], 'w')

    for line in input_file:
        # line = line.strip()
        args = line.strip().split('\t')
        event = args.pop(2)
        if event == START_REQUEST:
            req = Request(args[1], int(args[0]))
            request_map[args[1]] = req
        elif event == BACKEND_CONNECT:
            req = request_map[args[1]]
            backend = Backend(args[2], args[3])
            req.add_backend(backend)
            backends_map[backend.id] = backend            
        elif event == BACKEND_REQUEST:
            req = request_map[args[1]]
            req.failed_be_requests += 1
            backend = req.get_backend(args[2])
            backend.requests += 1
        elif event == BACKEND_OK:
            req = request_map[args[1]]
            req.failed_be_requests -= 1
        elif event == BACKEND_ERROR:
            req = request_map[args[1]]
            backend = req.get_backend(args[2])
            backend.errors[args[3]] = backend.errors.get(args[3], 0) + 1
        elif event == START_MERGE:
            pass
        elif event == START_SEND_RESULT:
            req = request_map[args[1]]
            req.start_send_time = int(args[0])
        elif event == FINISH_REQUEST:
            req = request_map[args[1]]
            req.total_time = int(args[0]) - req.start_request_time
            req.send_time = int(args[0]) - req.start_send_time
            request_times.append(req.total_time)
            send_times.append(req.send_time)

        # handler = event_handlers[event]
        # handler(*args)

    # from guppy import hpy
    # h = hpy()
    # print h.heap()

    # send_times.sort()
    # request_times.sort()
    # print backends_map.keys()
    # print request_times
    # print send_times
